import { configureStore } from "@reduxjs/toolkit";
import reducer from "./stateManagement/reducer";


const store = configureStore({
    reducer: {
        text: reducer,
    },
});

export default store;

