import * as T from './action-types';

interface State {
    text: string;
}

const initialState: State = {
    text: '',
}

export const Actions = Object.freeze({
    setData: (data: any) => ({ type: T.TEXT, payload: data })
});

const reducer = (state = initialState, action:any) => {

    switch (action.type) {
        case T.TEXT:
            return {
                ...state,
                text: action.payload,
            };
        default:
            return state;
    }
};
export default reducer;