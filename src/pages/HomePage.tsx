import { ComponentInput } from "../Components/ComponentInput";
import  ComponentOutput  from "../Components/ComponentOutput";

const Home = () => {
    return <div>
        <div className="Page-container">Главная страница
            <ComponentInput />
            <ComponentOutput />
        </div>
    </div>
};

export default Home;