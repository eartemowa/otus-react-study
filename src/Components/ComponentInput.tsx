import { Button, TextField } from "@mui/material";
import { useState } from "react";
import { connect, useDispatch } from "react-redux";
import { Actions } from "../stateManagement/reducer";

export function ComponentInput(){

    const dispatch = useDispatch();
    const [text, setText ] = useState<string>('');

    const click = () => {
        console.log(text)
        if (text) dispatch(Actions.setData(text))
    }
      
    return <div className="Component-container">
            <div className="item">
                <b>Первый компонент</b>
            </div>
            <div className="item">
                <TextField label="Введите значение" variant="outlined" onChange={e => setText(e.target.value)}></TextField>
            </div>
            <div className="item">
                <Button variant="contained" onClick={click}>Отправить во 2-й комп.</Button>
            </div>      
    </div>
}