import { TextField } from "@mui/material";
import { connect } from "react-redux";

interface Props {
    data?: any;
}

const ComponentOutput = (props: Props) => {
    console.log(props.data);
    return <div className="Component-container">
        <div><b>Второй компонент</b></div>
        <div className="item">
            <TextField disabled label="Было введено" variant="outlined" value={props.data}></TextField>
        </div>
    </div>
}

const mapStateToProps = (state: any) => {
    console.log(state.text);
    return { data: state.text.text };
}


export default connect(mapStateToProps)(ComponentOutput);