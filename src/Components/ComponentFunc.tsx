import { useState } from 'react';
import { Error } from './Error';
import axios from "axios";

interface Props{
    urlValue:string
}

export function ComponentFunc(props:Props){

    const [urlValue, setUrlValue] = useState<string>(props.urlValue);
    const [errorMessage, setError ] = useState<string>('');
    const [fact, setFact ] = useState<string>();

    const click = () => {
        //  console.log(urlValue);
          axios(urlValue)
          .then(response => {
              console.log(response.data)
              setFact(response.data?.fact)
          })
          .catch(error => {
              console.log(error)
              setError(error.message)
          })
          .then(()=>{
              console.log("Все готово");
          })
      }

    return <div>
        <input value={urlValue} onChange={e => setUrlValue(e.target.value)}/>
        <button onClick={click}>
                Отправить
        </button>
        <div className="App-request-result">{fact}</div>
        <Error message={errorMessage}></Error>
    </div>;
}