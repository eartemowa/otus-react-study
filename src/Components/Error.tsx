import { Component } from "react";
import './../App.css';

interface Props {
    message: string;
}

export class Error extends Component<Props> {
    render() {
        return <div className="App-error">
            {this.props.message}
        </div>;
    }
}