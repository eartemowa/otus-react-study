import axios from "axios";
import { Component, } from "react";
import { Error } from './Error';

interface State {
    urlValue: string;
    message: string;
    requestResult: any;
}

interface Props {
    urlValue: string;
}

export class ComponentClass extends Component<Props, State> {
    
    constructor (props: Props | Readonly<Props>) {
        super(props);
        this.state = {
            urlValue: props.urlValue,
            message: '',
            requestResult: ''
        };
      }

    click = () => {
         axios(this.state.urlValue)
        .then(response => {
            console.log(response.data)
            this.setState({ requestResult: response.data?.fact})
        })
        .catch(error => {
            console.log(error)
            this.setState({message: error.message})
        })
        .then(()=>{
            console.log("Все готово");
        })
    }

    render() {
        return <div>
            <input value={this.state.urlValue} onChange={e => this.setState({ urlValue: e.target.value})}/>
            <button onClick={this.click}>
                Отправить
            </button>
            <div className="App-request-result">{this.state.requestResult}</div>
            <Error message={this.state.message}></Error>
        </div>;
    }
}