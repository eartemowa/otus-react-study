import React from 'react';
import logo from './logo.svg';
import './App.css';
import { ComponentClass } from './Components/ComponentClass';
import { ComponentFunc } from './Components/ComponentFunc';
import { BrowserRouter, Link, Routes, Route } from 'react-router-dom';
import HomePage from './pages/HomePage';
import Login from './pages/Login';
import Register from './pages/Register';
import { Container, Nav, Navbar } from 'react-bootstrap';

function App() {
  return (
    <BrowserRouter>
      <Navbar bg="light" expand="lg">
          <Container>
            <Navbar.Collapse id="basic-navbar-nav">
              <Nav className="me-auto">
                <Nav.Link className="App-link" href="/">Home Page</Nav.Link>
                <Nav.Link className="App-link" href="/login">Login Page</Nav.Link>
                <Nav.Link className="App-link" href="/register">Register Page</Nav.Link>
              </Nav>
            </Navbar.Collapse>
          </Container>
        </Navbar>
      <Routes>
        <Route path='/' element={<HomePage />} />
        <Route path='/login' element={<Login />} />
        <Route path='/register' element={<Register />} />
        <Route path="*" element={<span>404</span>} />
      </Routes>
    </BrowserRouter>
/*      <div className="App">
      <ComponentClass urlValue = 'https://catfact.ninja/fact' />
      <ComponentFunc  urlValue = 'https://catfact.ninja/fact'/>
    </div>  */
  );
}

export default App;

